import os, sys
import random
import subprocess
import re
import time


def printing(content):
    print('\033[1;36;40m'+content+'\033[0;37;40m'+'\n')


def execute(cmd):
    execute_thread = subprocess.Popen(cmd.split(),stderr=subprocess.PIPE,stdout=subprocess.PIPE)
    output,error = execute_thread.communicate(input=cmd)
    if output:
            with open('/tmp/sysprep.stdout','a') as Stdout:
                Stdout.write('##############################################################\n')
                Stdout.write(cmd+'\n')
                Stdout.write(output+'\n')
                Stdout.write('command returncode'+' '+str(execute_thread.returncode))
                Stdout.write('\n##############################################################\n')

    if error:
            with open('/tmp/sysprep.stderr','a') as StdErr:
                StdErr.write('##############################################################\n')
                StdErr.write(cmd+'\n')
                StdErr.write(error+'\n')
                StdErr.write('command returncode'+'  '+str(execute_thread.returncode))
                StdErr.write('\n##############################################################\n')



##Setup Hostname randomly
printing('Setup Hostname randomly')
temp_hostname = 'localhost' + str(random.choice(xrange(200))) + '.local'
cmd_new_hostname = "sed -i s/^HOSTNAME=.*/HOSTNAME=%s/g /etc/sysconfig/network" % (temp_hostname)
execute(cmd_new_hostname)

##Wipe legacy NIC record
# cmd_empty_udev_nic = 'echo -n > /etc/udev/rules.d/70-persistent-net.rules'
printing('Wipe legacy NIC udev rules')
cmd_empty_udev_nic = 'dd if=/dev/null of=/etc/udev/rules.d/70-persistent-net.rules'
execute(cmd_empty_udev_nic)
# cmd_empty_nic_hwaddr = "sed -i -e  '/^HWADDR/d' -e '/^UUID/d' -e '/^DEVICE/d' -e '/^NAME/d' /etc/sysconfig/network-scripts/ifcfg-"


##Delete False NIC information file and Producing new files.
# cmd_empty_nic_hwaddr = "sed -i -e  /^HWADDR/d -e /^UUID/d -e /^DEVICE/d -e /^NAME/d /tmp/ifcfg-"
printing('Wipe legacy NIC ifcfg and create New NIC')
cmd_delete_false_nic = [os.remove('/etc/sysconfig/network-scripts/' + false_nic) for false_nic in
                        filter(lambda x: re.findall(r'ifcfg-ens\d+|ifcfg-eth\d+', x), os.listdir('/etc/sysconfig/network-scripts/'))]
discovered_nic = filter(lambda x: re.findall(r'ens\d+|eth\d+', x), os.listdir('/sys/class/net/'))
for nic in discovered_nic:
    try:
        nic_etc_file = open('/etc/sysconfig/network-scripts/ifcfg-' + nic, 'a')
        HWADDR = open('/sys/class/net/' + nic + '/address', 'r').read()
        line_1 = 'DEVICE=' + nic + '\n'
        line_2 = 'HWADDR=' + HWADDR
        line_3 = 'NAME=' + nic + '\n'
        line_4 = 'TYPE=Ethernet\n'
        line_5 = 'ONBOOT=yes\n'
        line_6 = 'BOOTPROTO=none\n'
        seq = [line_1, line_2, line_3, line_4, line_5, line_6]
        nic_etc_file.seek(0, 2)
        nic_etc_file.writelines(seq)
        nic_etc_file.close()
    except IOError:
        print 'Not found any valid NIC configuration file'

##Cleaning old installation log files.
printing('Cleaning old installation log files')
installation_log_files = ['/root/anaconda-ks.cfg', '/root/install.log', '/root/install.log.syslog']
for install_log in installation_log_files:
    try:
        with open(install_log, 'r') as f:
            os.remove(install_log)
            pass
    except IOError, OSError:
        print ('File doesn\'t exists')

##Allowing verbose message on boot
printing('Allowing verbose message on boot')
cmd_disable_quiet_boot = 'sed -i s/rhgb\|quiet//g /boot/grub/grub.conf'
execute(cmd_disable_quiet_boot)
##Stopping System service rsylog and auditd
printing('Stopping System service rsylog and auditd')
rsyslogd_stop = 'service rsyslog stop'
rsyslogd_start = 'service rsyslog start'
auditd_stop = 'service auditd stop'
auditd_start = 'service auditd start'
execute(rsyslogd_stop)
execute(auditd_stop)

###Cleaning up the logs.

# Delete old logs
printing('Cleaning up the log')
var_log = []
for dir_name in os.walk('/var/log'):
        var_log.append(dir_name)
for logs_n in range(len(var_log)):
    old_log = filter(lambda x: re.findall(r'\d+$',x),var_log[logs_n][2])
    for log_file in old_log:
        os.remove(var_log[logs_n][0]+'/'+log_file)
    current_log = filter(lambda x: re.findall(r'^\w+$',x),var_log[logs_n][2])
    current_log.extend(filter(lambda x:re.findall(r"(\.\w+$)",x),var_log[logs_n][2]))
    for LogFile in current_log:
        subprocess.call(['cp','/dev/null',var_log[logs_n][0]+'/'+LogFile])

execute(rsyslogd_start)
execute(auditd_start)
##Remove SSH host key
printing('Remove SSH host key')
os.system('rm -f /etc/ssh/*key*')

##Add Jump Host ssh public key as authorized keys

printing('Add Jump Host ssh public key as authorized keys')
try :
     fd = open('/root/.ssh/authorized_keys','a')
     fd.write('ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAgEAy2qZoU6XK4YiGIAjPRPnF49h4JrbqP8LZEM3JCdVnyXOLOTGrffyULJRCG6RZF8OwxQ5JK27FdgL7M6DTZnOZ0MfZMf7Iv+XNBOOTsEZpBZapH19Rw8zlZV85sEncdXQ8hkMiSOrzcMuWOGJT9p5/w8GoNl++gj4U0NOgOgLVuEZBCIc9szd3MPLAzLuGC4nvFwqvFW9A5Yql7oaNUZ1YDaJxqjJupJrrC7is7EeKE2neqW7M2268S2IPyabgIURlBMHjgQhddC6gZng7htSk5gVUoyiw/kS6mh52qv1AJYjD0DZn7ycSe63HPHX3OSzEIw4xgTTLhYI1JhocYee2RxYyW6Zt6+ay6JjaKOeIUztCGpptcDjN0tXcQLWCrYDdQfM9K3c+hRVrL1LCNjcqZkMCjN6sY0gAwKeo18EGZRL1JX3gwPVkvDXFGUvlYZca1FDkaPlyOZuq8+HVUWd5WEwaQkvIpfgA4dNQUnuXMOhLXWJY8OzQcEL1q5EUC4+DcV7fAvBQE/Zu0QGvH3yLAhpV9eI6jxGmqgOYIKJZB3l6DvpKiNENizIJSCsedyS2IyZ2j4MwXtuMuznDoWDmpiEJe3do93KptQXv+rD9TK/SclPhn+Zx9/UX8npCnUFsI3Akh2HSwrco8Pvoot2CKaUyX1TKuvU06urq3wO1dM= root@dc2puplorcqa01.mgmt.savvis.net')
except IOError:
     os.mkdir('/root/.ssh')
     fd = open('/root/.ssh/authorized_keys','a')
     fd.write('ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAgEAy2qZoU6XK4YiGIAjPRPnF49h4JrbqP8LZEM3JCdVnyXOLOTGrffyULJRCG6RZF8OwxQ5JK27FdgL7M6DTZnOZ0MfZMf7Iv+XNBOOTsEZpBZapH19Rw8zlZV85sEncdXQ8hkMiSOrzcMuWOGJT9p5/w8GoNl++gj4U0NOgOgLVuEZBCIc9szd3MPLAzLuGC4nvFwqvFW9A5Yql7oaNUZ1YDaJxqjJupJrrC7is7EeKE2neqW7M2268S2IPyabgIURlBMHjgQhddC6gZng7htSk5gVUoyiw/kS6mh52qv1AJYjD0DZn7ycSe63HPHX3OSzEIw4xgTTLhYI1JhocYee2RxYyW6Zt6+ay6JjaKOeIUztCGpptcDjN0tXcQLWCrYDdQfM9K3c+hRVrL1LCNjcqZkMCjN6sY0gAwKeo18EGZRL1JX3gwPVkvDXFGUvlYZca1FDkaPlyOZuq8+HVUWd5WEwaQkvIpfgA4dNQUnuXMOhLXWJY8OzQcEL1q5EUC4+DcV7fAvBQE/Zu0QGvH3yLAhpV9eI6jxGmqgOYIKJZB3l6DvpKiNENizIJSCsedyS2IyZ2j4MwXtuMuznDoWDmpiEJe3do93KptQXv+rD9TK/SclPhn+Zx9/UX8npCnUFsI3Akh2HSwrco8Pvoot2CKaUyX1TKuvU06urq3wO1dM= root@dc2puplorcqa01.mgmt.savvis.net')


##Removing root and other user history
printing('Removing root and other user history')
os.system('find /root -name .bash_history -exec rm {} \;')
os.system('find /home -name .bash_history -exec rm {} \;')

##Restart SSHD Daemon
sshd_restart = 'service sshd restart'
execute(sshd_restart)

##Cleaning yum metadata
printing('Cleaning yum metadata')

yum_clean = 'yum clean all'
execute(yum_clean)

##Cleanup RHN registration
printing('Cleanup RHN registration')
try :
     os.remove('/etc/sysconfig/rhn/systemid')
except OSError as e:
     print ('system not registered with any RHN')

##customize vmware-tools
custom_vmtools = '/usr/bin/vmware-config-tools.pl -d'
execute(custom_vmtools)

##Delay the reboot for 15 Sec
time.sleep(15)

##Reboot system after customization
reboot = 'init 6'
execute(reboot)
